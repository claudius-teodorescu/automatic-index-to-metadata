use clap::{Arg, Command};
use lazy_static::lazy_static;
use lexical_sort::{natural_lexical_cmp, StringSort};
use oxigraph::io::RdfFormat;
use oxigraph::store::Store;
use regex::Regex;
use sophia::api::graph::Graph;
use sophia::api::ns::Namespace;
use sophia::api::term::Term;
use sophia::api::term::{matcher::Any, IriRef, SimpleTerm};
use sophia::api::{prelude::*, MownStr};
use sophia::inmem::graph::LightGraph;
use sophia::turtle::parser::turtle;
use sophia::turtle::serializer::turtle::TurtleSerializer;
use std::collections::{BTreeSet, HashSet};
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use unicode_normalization::UnicodeNormalization;

const ONTOLOGY_BASE_IRI: &str = "https://kuberam.ro/ontologies/text-index#";
const ALL_CHARACTERS_PROPERTY: &str = "allCharacters";
const CHARACTER_NORMALIZATION_MAPPING_PROPERTY: &str = "characterNormalisationMapping";
const METADATA_CONSTRUCT_QUERY_PROPERTY: &str = "metadataConstructQuery";

lazy_static! {
    static ref MAPPINGS_FORMAT_REGEX: Regex = Regex::new(r#"^(\S=\S,)*(\S=\S)$"#).unwrap();
}

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("triples_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(
            Arg::new("metadata_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(2)
                .required(true),
        )
        .arg(Arg::new("metadata_url").index(3).required(true))
        .get_matches();

    let triples_file_path: &PathBuf = cli_matches
        .get_one("triples_file_path")
        .expect("`triples_file_path`is required");
    let metadata_file_path: &PathBuf = cli_matches
        .get_one("metadata_file_path")
        .expect("`metadata_file_path`is required");
    let metadata_url: &String = cli_matches
        .get_one("metadata_url")
        .expect("`metadata_url`is required");

    generate_metadata(triples_file_path, metadata_file_path, metadata_url);
}

fn generate_metadata(
    triples_file_path: &PathBuf,
    metadata_file_path: &PathBuf,
    metadata_url: &str,
) {
    let mut triples_file_folder = PathBuf::from(triples_file_path);
    triples_file_folder.pop();

    // load the triples
    let triples_string = fs::read_to_string(triples_file_path).expect("cannot load metadata file");
    let triples = triples_string.as_str();
    let store = load_triples(triples_file_path);

    // load the metadata as graph
    let mut metadata_graph: LightGraph = load_metadata_graph(metadata_file_path);
    let metadata_iri_namespace = Namespace::new(metadata_url).unwrap();
    let metadata_iri_term = metadata_iri_namespace.iri().unwrap();
    let ontology_namespace = Namespace::new(ONTOLOGY_BASE_IRI).expect("cannot create namespace");

    // generate default metadata
    generate_default_metadata(
        &mut metadata_graph,
        &metadata_iri_term,
        &ontology_namespace,
        triples,
    );

    // generate custom metadata
    let custom_metadata_graph = generate_custom_metadata(&mut metadata_graph, &store);

    // serializing the metadata graph
    let mut turtle_stringifier = TurtleSerializer::new_stringifier();
    let mut output_metadata = turtle_stringifier
        .serialize_graph(&metadata_graph)
        .expect("cannot serialize graph")
        .to_string();

    output_metadata.push_str(custom_metadata_graph.as_str());

    //println!("{}", output_metadata);
    fs::write(triples_file_folder.join("metadata.ttl"), &output_metadata)
        .expect("cannot write file.");
}

fn load_triples(file_path: &PathBuf) -> Store {
    let store = Store::new().unwrap();

    // load the triples
    let triples_file = File::open(file_path).expect("cannot open the Turtle triples file");
    let reader = BufReader::new(triples_file);
    let _ = store.load_from_reader(RdfFormat::Turtle, reader);

    store
}

fn generate_default_metadata(
    metadata_graph: &mut LightGraph,
    metadata_iri_term: &IriRef<MownStr>,
    ontology_namespace: &Namespace<&str>,
    triples: &str,
) {
    // extract the default metadata
    let triples_graph: LightGraph = turtle::parse_str(triples)
        .collect_triples()
        .expect("cannot extract triples");

    let objects = triples_graph.objects();

    let mut all_characters_set: BTreeSet<String> = BTreeSet::new();

    for object in objects.into_iter() {
        match object.unwrap().lexical_form().unwrap() {
            object_lexical_form => {
                let object_value_chars = object_lexical_form.chars();
                for character in object_value_chars.into_iter() {
                    all_characters_set.insert(character.to_string().to_lowercase());
                }
            }
        }
    }

    // sort all the characters
    let mut all_characters: Vec<String> = Vec::from_iter(all_characters_set);
    all_characters.string_sort_unstable(natural_lexical_cmp);

    // generate the default search groups
    let mut character_normalization_mappings: HashSet<String> = HashSet::new();
    for character in all_characters.iter() {
        let normalised_character: String = character
            .clone()
            .as_str()
            .nfd()
            .filter(char::is_ascii)
            .collect();

        if normalised_character.len() != 0 && normalised_character != *character {
            character_normalization_mappings
                .insert(format!("{}={}", character, normalised_character));
        }
    }

    // add all characters triple
    metadata_graph
        .insert(
            metadata_iri_term.clone(),
            ontology_namespace
                .get(ALL_CHARACTERS_PROPERTY)
                .expect("cannot create term"),
            all_characters.join(",").as_str(),
        )
        .expect("cannot insert triple");

    // add search group triples
    for mapping in character_normalization_mappings.into_iter() {
        metadata_graph
            .insert(
                metadata_iri_term.clone(),
                ontology_namespace
                    .get(CHARACTER_NORMALIZATION_MAPPING_PROPERTY)
                    .expect("cannot create term"),
                mapping.as_str(),
            )
            .expect("cannot insert triple");
    }
}

fn generate_custom_metadata(metadata_graph: &mut LightGraph, store: &Store) -> String {
    let construct_queries = _get_construct_queries(metadata_graph);
    let mut results = "".to_string();

    // run the SPARQL queries against the triples store
    for construct_query in construct_queries.into_iter() {
        let mut result_bytes = Vec::new();

        let _ = store
            .query(construct_query.as_str())
            .unwrap()
            .write_graph(&mut result_bytes, RdfFormat::Turtle);

        let result = std::str::from_utf8(&result_bytes).unwrap();

        results.push_str(result);
    }

    results
}

fn load_metadata_graph(metadata_file_path: &PathBuf) -> LightGraph {
    // load the metadata as string
    let input_metadata = fs::read_to_string(metadata_file_path).expect("cannot load metadata file");

    // load the metadata as graph
    let metadata_graph: LightGraph = turtle::parse_str(input_metadata.as_str())
        .collect_triples()
        .expect("cannot extract triples");

    metadata_graph
}

fn _get_construct_queries(metadata_graph: &mut LightGraph) -> Vec<String> {
    let mut construct_queries: Vec<String> = Vec::new();

    let construct_query_predicate = SimpleTerm::Iri(IriRef::new_unchecked(
        [ONTOLOGY_BASE_IRI, METADATA_CONSTRUCT_QUERY_PROPERTY]
            .concat()
            .into(),
    ));
    let construct_query_triples =
        metadata_graph.triples_matching(Any, Some(construct_query_predicate), Any);
    for construct_query_triple in construct_query_triples.into_iter() {
        let construct_query_term = construct_query_triple.unwrap()[2];

        if let Some(construct_query_lexical_form) = construct_query_term.lexical_form() {
            let construct_query_simple_term: &str = construct_query_lexical_form.borrow_term();

            construct_queries.push(construct_query_simple_term.to_string());
        }
    }

    construct_queries
}

#[test]
pub fn test_generate_metadata() {
    let index_name = "headwords";
    let triples_file_path = format!("/home/claudius/workspace/repositories/git/gitlab.com/solirom-citada/data/public/peritext/indexes/{}/index.ttl", index_name);
    let metadata_file_path = format!("/home/claudius/workspace/repositories/git/gitlab.com/solirom-citada/data/contents/peritext/indexes/{}/metadata.ttl", index_name);
    let metadata_url = format!(
        "https://solirom-citada.gitlab.io/data/peritext/indexes/{}/metadata.ttl",
        index_name
    );

    generate_metadata(
        &PathBuf::from(triples_file_path),
        &PathBuf::from(metadata_file_path),
        metadata_url.as_str(),
    );
}
// time ./index-as-triples-to-metadata "/home/claudius/workspace/repositories/git/gitlab.com/solirom-citada/data/public/peritext/indexes/headwords/index.ttl" "/home/claudius/workspace/repositories/git/gitlab.com/solirom-citada/data/contents/peritext/indexes/headwords/metadata.ttl" "https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/metadata.ttl"
